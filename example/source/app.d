import autoimpl;
import std.stdio;

extern (C):

struct MyStruct
{
    int a;
    int b;
}

auto foo(MyStruct t) @safe @nogc pure nothrow
{
    return t.a + t.b;
}

auto ident(MyStruct t) @safe @nogc pure nothrow
{
    return t;
}

mixin MixinParameters!MyStruct;

void main()
{
    printf("%d\n", atosed!foo(10.A, 20.B));

    mixin Unpack!(["", "b"], ident(MyStruct(10, 20)));
    printf("%d\n", b);
}
