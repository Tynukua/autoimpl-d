module autoimpl.parameters;
import std.ascii : toUpper, isLower;
import std.traits : FieldNameTuple, Fields, Parameters;

enum NewFields(T) = () {
    alias fields = FieldNameTuple!T;
    string[] fs = [];
    static foreach (f; fields)
    {
        static assert(f[0].isLower, "Field must be in camel case, and  starts from lower letter");
        fs ~= (f[0].toUpper ~ f[1 .. $]);
    }
    return fs;
}();
/// Used to create types for "named" params
/// Params:
/// T = struct 
mixin template MixinParameters(T)
{
    import std.traits : FieldNameTuple, Fields, Parameters;

    alias fields = FieldNameTuple!T;
    alias fieldTypes = Fields!T;
    enum newFields = NewFields!T;
    static foreach (i, f; newFields)
    {
        mixin(`
            struct ` ~ f ~ `
            {
                this(__T)(__T __value)
                {
                    this.value = __value;
                }
            ` ~ fieldTypes[i].stringof ~ ` value;
            }`);
    }
}

///
unittest
{
    struct S
    {
        int i;
    }

    mixin MixinParameters!S;
    I i = I(42);
    assert(i.value == 42);
}

/// Used for get struct from variadic arguments
/// Params:
///   T = Type of struct 
///   argv = fields of struct
/// Returns: struct of T type field by argv values;
T atos(T, ARGS...)(ARGS argv)
{
    import std.algorithm : countUntil;

    T _t;
    enum fields = FieldNameTuple!T;
    enum newFields = NewFields!T;
    static foreach (i, A; ARGS)
    {

        static if (newFields.countUntil(A.stringof) >= 0)
        {
            mixin("_t." ~ fields[newFields.countUntil(A.stringof)] ~ " = argv[i].value;");
        }
        else
            pragma(msg, "Warning: " ~ A.stringof ~ "is not a valid field name! Value skiped");
    }
    return _t;
}

///
unittest
{
    struct MyStruct
    {
        int i;
        float f = 0.0f;
        string s;
    }

    mixin MixinParameters!MyStruct;

    assert(atos!MyStruct(I(42)) == MyStruct(42, 0, ""));
    assert(atos!MyStruct(F(42)) == MyStruct(0, 42, ""));
    assert(atos!MyStruct(S("42")) == MyStruct(0, 0, "42"));
    assert(atos!MyStruct(S("42"), I(42)) == MyStruct(42, 0, "42"));
    assert(atos!MyStruct(F(42), S("42"), I(42)) == MyStruct(42, 42, "42"));

}
/// Used to create function whith "Named" arguments
/// Params:
///   func = function/delegate which accept as param 1 struct
///   argv = arumments of new function
auto atosed(alias func, ARGS...)(ARGS argv)
{
    alias params = Parameters!func;
    static assert(params.length == 1, "Function must have one parameter");
    alias T = params[0];
    static assert(is(T == struct), "Functions must accept struct argument");
    return func(argv.atos!T);
}

///
unittest
{
    struct MyStruct
    {
        int i;
        string s;
    }

    mixin MixinParameters!MyStruct;

    auto str(MyStruct m)
    {
        import std.conv : to;

        return m.to!string;
    }

    assert(atosed!str(I(42)) == str(MyStruct(42, "")));
    assert(atosed!str(S("42"), I(42)) == str(MyStruct(42, "42")));
}

unittest
{
    /// Arguments of function printS
    struct S
    {
        int i;
        int arg2;
        string sv;
    }

    string printS(S s)
    {
        import std.conv : to;

        return s.to!string;
    }

    mixin MixinParameters!S;

    auto v1 = I(1);
    auto v2 = Arg2(2);
    auto v3 = Sv("string");
    assert(v1.value == 1);
    assert(v2.value == 2);
    assert(v3.value == "string");
    S s = atos!S(v1, v2, v3);
    assert(s == S(1, 2, "string"));
    assert(atosed!printS(v1, v2, v3) == printS(s));
    assert(atosed!printS(v3, v2, v1) == printS(s));

    assert(atosed!printS(I(1), Sv("data")) == printS(S(1, 0, "data")));
    assert(atosed!printS(Sv("data")) == printS(S(0, 0, "data")));
    assert(atosed!printS(Arg2(100)) == printS(S(0, 100, "")));
    assert(atosed!printS() == printS(S(0, 0, "")));
}
