module autoimpl.unpack;

mixin template Unpack(string[] fields, alias v)
{
    static foreach (i, f; fields)
    {
        static if (f != "")
            mixin("auto " ~ f ~ " = v.tupleof[i];");
    }
}

unittest
{
    struct S
    {
        int a;
        float m;
        string t;
    }

    {
        mixin Unpack!(["age", "", "title"], S(42, 0.0f, "str"));
        assert(title == "str");
        assert(age == 42);
    }
    {
        import std: tuple;
        mixin Unpack!(["age", "", "title"], tuple(42, 0.0f, "str"));
        assert(title == "str");
        assert(age == 42);
    }
    {
        auto s = S(42, 0, "str");
        auto title = s.t;
        auto age = s.a;
        assert(title == "str");
        assert(age == 42);

    }
    
}
