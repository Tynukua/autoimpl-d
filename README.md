
# autoimpl

## Auto implementing named parameters
```d
import autoimpl.parameters;

void myFync(S);

struct S{int param1, param2};

mixin NamedParameters!S;

atosed!myFync(Param(2), Param1(10));
```

## TDOO
- mv autoint into autoimpl.interfaces
- fix some autoint's bugs
- More tests for named parameters
